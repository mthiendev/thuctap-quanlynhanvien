const getStaffFromForm = () => {
  const userName = document.getElementById("tknv").value;
  const name = document.getElementById("name").value;
  const email = document.getElementById("email").value;
  const password = document.getElementById("password").value;
  const workday = document.getElementById("datepicker").value;
  const basicSalary = document.getElementById("luongCB").value * 1;
  const position = document.getElementById("chucvu").value * 1;
  const workTime = document.getElementById("gioLam").value;
  return new Staff(userName, name, email, password, workday, basicSalary, position, workTime);
};
const editStaffInfor = (userName) => {
  const index = findIndexOfStaffs(userName);
  if (index !== -1) {
    document.getElementById("tknv").value = arrStaffs[index].userName;
    document.getElementById("name").value = arrStaffs[index].name;
    document.getElementById("email").value = arrStaffs[index].email;
    document.getElementById("password").value = arrStaffs[index].password;
    document.getElementById("luongCB").value = arrStaffs[index].basicSalary;
    document.getElementById("chucvu").value = arrStaffs[index].positionVal;
    document.getElementById("gioLam").value = arrStaffs[index].workTime;
    $("#myModal").modal("show");
    document.getElementById("tknv").disabled = true;
  }
};
const updateStaff = () => {
  let newStaff = getStaffFromForm();
  let index = findIndexOfStaffs(newStaff.userName);
  console.log(index);
  if (index !== -1) {
    arrStaffs[index] = newStaff;
  }
  renderStaffs(arrStaffs);
  saveInLocalStorage(arrStaffs, LOCAL_STORAGE_STAFFS);
};
const resetValueForm = () => {
  document.getElementById("tknv").value = "";
  document.getElementById("name").value = "";
  document.getElementById("email").value = "";
  document.getElementById("password").value = "";
  document.getElementById("luongCB").value = "";
  document.getElementById("chucvu").value = "";
  document.getElementById("gioLam").value = "";
};
const renderStaffs = (arr) => {
  let content = "";
  arr.forEach((staff, index) => {
    content += `<tr>
    <td>${staff.userName}</td>
    <td>${staff.name}</td>
    <td>${staff.email}</td>
    <td>${staff.workday}</td>
    <td>${staff.getPosition()}</td>
    <td>${staff.getSalary()}</td>
    <td>${staff.ratings()}</td>
    <td>
    <button class="btn btn-info mx-1" onclick="editStaffInfor('${staff.userName}')" >Sửa</button>
    <button class="btn btn-danger my-1" onclick="removeStaff('${staff.userName}')">Xoá</button>
  </td>    
    </tr>`;
  });
  document.getElementById("tableDanhSach").innerHTML = content;
};
const removeStaff = (userName) => {
  const index = findIndexOfStaffs(userName);
  console.log(index);
  if (index !== -1) {
    arrStaffs.splice(index, 1);
    renderStaffs(arrStaffs);
    saveInLocalStorage(arrStaffs, LOCAL_STORAGE_STAFFS);
  }
};
const saveInLocalStorage = (arr, name) => {
  localStorage.setItem(name, JSON.stringify(arr));
};
const findIndexOfStaffs = (userName) => {
  return arrStaffs.findIndex((staff) => {
    return userName === staff.userName;
  });
};
const findName = () => {
  let newArr = [];
  const nameSearach = document.getElementById("searchName").value;
  arrStaffs.forEach((staff, index) => {
    console.log(staff.name.includes(nameSearach));
    if (staff.name.includes(nameSearach)) {
      newArr = [...newArr, staff];
    }
  });
  renderStaffs(newArr);
  console.log(newArr);
};
