class Staff {
  constructor(_userName, _name, _email, _password, _workday, _basicSalary, _positionVal, _workTime) {
    this.userName = _userName;
    this.name = _name;
    this.email = _email;
    this.password = _password;
    this.workday = _workday;
    this.basicSalary = _basicSalary;
    this.positionVal = _positionVal;
    this.workTime = _workTime;
  }
  getPosition() {
    switch (this.positionVal) {
      case 1:
        return "Nhân viên";
      case 2:
        return "Trưởng phòng";
      case 3:
        return "Giám đóc";
    }
  }
  getSalary() {
    return this.basicSalary * this.positionVal;
  }

  ratings() {
    if (this.workTime < 160) {
      return "Trung bình";
    } else if (this.workTime >= 160 && this.workTime < 176) {
      return "Khá";
    } else if (this.workTime >= 176 && this.workTime < 192) {
      return "Giỏi";
    } else {
      return "Xuất sắc";
    }
  }
}
