const LOCAL_STORAGE_STAFFS = "LOCAL_STORAGE_STAFFS";
let arrStaffs = [];

const staffsStorage = localStorage.getItem(LOCAL_STORAGE_STAFFS);
if (staffsStorage) {
  newArrStaffs = JSON.parse(staffsStorage);
  newArrStaffs.forEach((staff, index) => {
    const newStaff = new Staff(staff.userName, staff.name, staff.emai, staff.password, staff.workday, staff.basicSalary, staff.positionVal, staff.workTime);
    arrStaffs = [...arrStaffs, newStaff];
  });
  renderStaffs(arrStaffs);
}

document.getElementById("btnThemNV").addEventListener("click", () => {
  const newStaff = getStaffFromForm();
  arrStaffs = [...arrStaffs, newStaff];
  renderStaffs(arrStaffs);
  saveInLocalStorage(arrStaffs, LOCAL_STORAGE_STAFFS);
  resetValueForm();
  $("#myModal").modal("hide");
});
document.getElementById("btnCapNhat").addEventListener("click", () => {
  updateStaff();
});
document.getElementById("btnTimNV").addEventListener("click", () => {
  findName();
});

document.getElementById("searchName").addEventListener("input", findName);
